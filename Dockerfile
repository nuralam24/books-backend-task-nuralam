
FROM node:18-alpine as base

WORKDIR /books

COPY package*.json ./

RUN npm install

COPY --chown=node:node . .

RUN npm run build

#USER node

EXPOSE 4411

#RUN export NODE_ENV=production

CMD [ "node", "dist/index.js" ]

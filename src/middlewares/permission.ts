const permission = (...allowed: any) => {
  const isAllowed = (role: any) => allowed.map((v: any )=> v.toLowerCase()).indexOf(role.toLowerCase()) > -1;
  return (req: any, res: any, next: any) => {
    if (req.user && isAllowed(req.user.role)) {
      next();
    } else {
      res.status(401).json({
        status: false,
        message: "Forbidden. Permission Denied!"
      });
    }
  };
};

export default permission;


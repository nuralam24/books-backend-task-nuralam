import { Schema, Document, model } from 'mongoose';

interface IUser extends Document {
  username: string;
  password: string;
  role: 'guest' | 'user' | 'admin';
}

const userSchema: Schema<IUser> = new Schema<IUser>({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: ['guest', 'user', 'admin'],
    default: 'guest'
  }
});

const User = model<IUser>('User', userSchema);
export default User;

import { Schema, Document, model, Types } from 'mongoose';

interface IBook extends Document {
  title: string;
  publicationDate: Date;
  genres: string[];
  author: Types.ObjectId;
}

const bookSchema: Schema<IBook> = new Schema<IBook>({
  title: {
    type: String,
    required: true
  },
  publicationDate: {
    type: Date,
    required: true
  },
  genres: [{
    type: String,
    required: true
  }],
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Author',
    required: true
  }
});

const Book = model<IBook>('Book', bookSchema);
export default Book;

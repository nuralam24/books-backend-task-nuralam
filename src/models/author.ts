import { Schema, Document, model } from 'mongoose';

interface IAuthor extends Document {
  name: string;
  biography?: string;
  birthDate: Date;
  books: Schema.Types.ObjectId[];
  createdAt?: Date;
  updatedAt?: Date;
}

const authorSchema: Schema<IAuthor> = new Schema<IAuthor>({
  name: {
    type: String,
    required: true
  },
  biography: {
    type: String
  },
  birthDate: {
    type: Date,
    required: true
  },
  books: [{
    type: Schema.Types.ObjectId,
    ref: 'Book'
  }]
}, {
  timestamps: true
});

const Author = model<IAuthor>('Author', authorSchema);
export default Author;

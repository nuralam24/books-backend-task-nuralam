import authorModel from './../models/author';
import { NextFunction, Request, Response } from "express";
import dotenv from 'dotenv';
dotenv.config();

const projection = {
    updatedAt: 0,
    __v: 0
};

// get all authors
const allAuthors = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const authors = await authorModel.find({}, projection).sort({ createdAt: -1 }).populate({
            path: 'books',
            select: 'title publicationDate genres',
            options: { strictPopulate: false }
        });
        if(!authors.length) {
            return res.status(200).json({
                data: [],
                success: true,
                message: 'No authors available!'
            });
        }
        return res.status(200).json({
            data: authors,
            success: true,
            message: 'All authors!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// get authors by id
const authorView = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const author = await authorModel.findById({_id: req.params.id}, projection).populate({
            path: 'books',
            select: 'title publicationDate genres',
            options: { strictPopulate: false }
        });
        if(!author) {
            return res.status(404).json({
                data: {},
                success: false,
                message: 'Author not found!'
            });
        }
        return res.status(200).json({
            data: author,
            success: true,
            message: 'author view!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// author create
const authorCreate = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const createNewAuthor = await authorModel.create(req.body);
        if(!createNewAuthor) {
            return res.status(400).json({
                data: null,
                success: false,
                message: `Author is not created!`
            });
        }
        const data = {
            _id: createNewAuthor._id, name: createNewAuthor.name,
            biography: createNewAuthor.biography, birthDate: createNewAuthor.birthDate
        };
        return res.status(201).json({
            data,
            success: true,
            message: `Author created successfully!`
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

export { allAuthors, authorView, authorCreate };

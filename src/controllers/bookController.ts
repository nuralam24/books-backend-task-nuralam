import bookModel from './../models/book';
import authorModel from './../models/author';
import { NextFunction, Request, Response } from "express";
import dotenv from 'dotenv';
dotenv.config();

const projection = {
    updatedAt: 0,
    __v: 0
};

// get all books
const allBooks = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const books = await bookModel.find({}, projection).sort({ createdAt: -1 }).populate({
            path: 'author',
            select: 'name biography birthDate',
            options: { strictPopulate: false }
        });

        if(!books.length) {
            return res.status(200).json({
                data: [],
                success: true,
                message: 'No books available!'
            });
        }
        return res.status(200).json({
            data: books,
            success: true,
            message: 'All books!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// get books by id
const bookView = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const book = await bookModel.findById({_id: req.params.id}, projection).populate({
            path: 'author',
            select: 'name biography birthDate',
            options: { strictPopulate: false }
        });

        if(!book) {
            return res.status(404).json({
                data: {},
                success: false,
                message: 'Book not found!'
            });
        }
        return res.status(200).json({
            data: book,
            success: true,
            message: 'book view!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// book create
const bookCreate = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const author = await authorModel.findById({_id: req.body.authorId}, {name: 1});
        if (!author) {
            return res.status(404).json({
                data: {},
                success: false,
                message: 'Author not found!'
            });
        }

        req.body.author = req.body.authorId;
        const createNewBook = await bookModel.create(req.body);
        if(!createNewBook) {
            return res.status(400).json({
                data: null,
                success: false,
                message: `Book is not created!`
            });
        }
        const data = {
            _id: createNewBook._id, title: createNewBook.title,
            publicationDate: createNewBook.publicationDate, genres: createNewBook.genres
        };
        return res.status(201).json({
            data,
            success: true,
            message: `Book created successfully!`
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

export { allBooks, bookView, bookCreate };

import userRouter from './user';
import authorRouter from './author';
import bookRouter from './book';

const routes = [
    { path: '/user', controller: userRouter },
    { path: '/authors', controller: authorRouter },
    { path: '/books', controller: bookRouter },
];

const setupRoutes = (app: any) => {
    for (const route of routes) {
        app.use(`/api/v1${route.path}`, route.controller);
    }
};

export default setupRoutes;

import { Router } from 'express';
import verifyToken from '../middlewares/verifyToken';
import permission from '../middlewares/permission';
import {bookCreate, allBooks, bookView} from '../controllers/bookController';
import { bookCreateValidator, bookViewValidator } from "../validations/bookValidation";

const router = Router();

router.get('/', [verifyToken, permission('admin', 'guest', 'user')], allBooks);
router.get('/:id', [bookViewValidator, verifyToken, permission('admin', 'guest', 'user')], bookView);
router.post('/', [bookCreateValidator, verifyToken, permission('admin')], bookCreate);

export default router;
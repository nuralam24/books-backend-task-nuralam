import { Router } from 'express';
import verifyToken from '../middlewares/verifyToken';
import permission from '../middlewares/permission';
import {authorCreate, allAuthors, authorView} from '../controllers/authorController';
import { authorCreateValidator, authorViewValidator } from "../validations/authorValidation";

const router = Router();

router.get('/', [verifyToken, permission('admin', 'guest', 'user')], allAuthors);
router.get('/:id', [authorViewValidator, verifyToken, permission('admin', 'guest', 'user')], authorView);
router.post('/', [authorCreateValidator, verifyToken, permission('admin')], authorCreate);

export default router;
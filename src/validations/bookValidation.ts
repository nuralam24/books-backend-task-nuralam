import Joi from "joi";
import { validationHandler } from "../utils/validationHandler";

const bookCreate = Joi.object({
    title: Joi.string().required(),
    publicationDate: Joi.date().required(),
    genres: Joi.array().items(Joi.string()).required(),
    authorId: Joi.string().hex().length(24).required()
});

const bookView = Joi.object({
    id: Joi.string().hex().length(24).required(),
});

const bookCreateValidator = validationHandler({
  body: bookCreate,
});

const bookViewValidator = validationHandler({
    params: bookView,
  });

export { bookCreateValidator, bookViewValidator };
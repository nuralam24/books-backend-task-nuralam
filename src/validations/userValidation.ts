import Joi from "joi";
import { validationHandler } from "../utils/validationHandler";

const registration = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required().min(5).max(50),
    role: Joi.string().required().valid('guest', 'user', 'admin')
});

const login = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required()
});

const registrationValidator = validationHandler({
  body: registration,
});

const loginValidator = validationHandler({
    body: login,
  });

export { registrationValidator, loginValidator };
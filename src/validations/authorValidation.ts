import Joi from "joi";
import { validationHandler } from "../utils/validationHandler";

const authorCreate = Joi.object({
    name: Joi.string().required(),
    biography: Joi.string().optional(),
    birthDate: Joi.date().required(),
    books: Joi.array().items(Joi.string()).optional()
});

const authorView = Joi.object({
    id: Joi.string().hex().length(24).required(),
});

const authorCreateValidator = validationHandler({
  body: authorCreate,
});

const authorViewValidator = validationHandler({
    params: authorView,
  });

export { authorCreateValidator, authorViewValidator };
# Book API

## Description
A RESTful API for managing books and authors, built with Node.js, TypeScript, Express, and MongoDB. Includes JWT authentication & Authorization.

## Features
- Create, View books and authors
- JWT-based authentication
- Role-based access control

## Setup

### Prerequisites
- Docker
- Docker Compose

### Manual Installation

1. Clone the repository
   ```bash
   git clone https://github.com/nuralam24/books-backend-task-nuralam.git
   cd books-backend-task-nuralam
   .env file must be added to the root directory. requested to follow .env.example file
   npm install
   npm run dev


### API ENDPOINT
# Auth
- POST: `http://localhost:4411/api/v1/user/registration`
- POST: `http://localhost:4411/api/v1/user/login`

# Author
- GET: `http://localhost:4411/api/v1/authors` (Requires Authentication & Authorization)
- GET: `http://localhost:4411/api/v1/authors/:id` (Requires Authentication & Authorization)
- POST: `http://localhost:4411/api/v1/authors` (Requires Authentication & Authorization)

# Book
- GET: `http://localhost:4411/api/v1/books` (Requires Authentication & Authorization)
- GET: `http://localhost:4411/api/v1/books/:id` (Requires Authentication & Authorization)
- POST: `http://localhost:4411/api/v1/books` (Requires Authentication & Authorization)